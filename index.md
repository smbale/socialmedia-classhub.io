---
title: Social Media and Text Analytics - Summer 2015 - University of Pennsylvania
layout: default
img: nyc_twitter_visualization
img_link: http://bits.blogs.nytimes.com/2011/07/15/bits-pics-showing-the-location-of-tweets-and-flickr-photos/
caption: A visualization showing the location of Twitter messages (blue) and Flickr photos (orange) in New York City by Eric Fischer
active_tab: main_page 
---


Social media provides a massive amount of valuable information and shows us how language is actually used by lots of people. This course will give an overview of prominent research findings on language use in social media. The course will also cover statistical analysis methods and off-the-shelf tools for obtaining and processing Twitter data.


Instructor
: &nbsp;&nbsp;&nbsp;&nbsp; [Wei Xu](http://www.cis.upenn.edu/~xwe/) is a post-doctoral researcher in the Computer and Information
Science Department at the University of Pennsylvania. Her research
interests are in social media and natural language processing. She has received her
PhD in 2014 from New York University, and is a recipient of the
MacCracken Fellowship. During her PhD, she visited University of
Washington for two years. She is organizing the ACL Workshop on Noisy
User-generated Text.


Time 
: &nbsp;&nbsp;&nbsp;&nbsp; 2-4pm | July 13,14,15,16 \| 2015 


Place
: &nbsp;&nbsp;&nbsp;&nbsp; University of Pennsylvania <br>
&nbsp;&nbsp;&nbsp;&nbsp;  IRCS Fishbowl \([directions](https://www.ircs.upenn.edu/about/directions-ircs)\)


Prerequisites
: &nbsp;&nbsp;&nbsp;&nbsp; There is no prerequisites for the class. Most content will be accessible for undergraduate and graduate students with any background. 

Course Readings
: &nbsp;&nbsp;&nbsp;&nbsp; [Various academic papers](syllabus.html)


